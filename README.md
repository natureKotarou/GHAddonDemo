# GHAddonDemo

Demo of grasshopper addon with custom Goo, Param, and Component.

# Requirements

- Rhino7
- build environment for .NET Framework 4.8

# Installation

1. Clone and build.
1. Place GHAddonDemo.gha into Grasshopper's Library directory (by default C:\Users\{UserName}\AppData\Roaming\Grasshopper\Libraries) or register build directory to Grasshopper using "GrasshopperDeveloperSettings" command on Rhino.
1. Launch Rhino and Grasshopper

# Usage

This demo contains only one Param and one Component.
Try Examples/example.gh.
