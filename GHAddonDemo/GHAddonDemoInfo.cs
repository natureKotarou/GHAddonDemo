﻿using Grasshopper;
using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace GHAddonDemo
{
    public class GHAddonDemoInfo : GH_AssemblyInfo
    {
        public override string Name => "GHAddonDemo";

        //Return a 24x24 pixel bitmap to represent this GHA library.
        public override Bitmap Icon => null;

        //Return a short string describing the purpose of this GHA library.
        public override string Description => "";

        public override Guid Id => new Guid("DE422941-000E-49CE-9A8B-39E082C23E1B");

        //Return a string identifying you or your company.
        public override string AuthorName => "";

        //Return a string representing your preferred contact details.
        public override string AuthorContact => "";
    }
}