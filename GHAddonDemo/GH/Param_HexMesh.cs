﻿using Grasshopper.Kernel;
using System;
using Rhino.Geometry;

namespace GHAddonDemo.GH
{
    public class Param_HexMesh : GH_Param<GH_HexMesh>, IGH_PreviewObject
    {
        public Param_HexMesh() : base("HexMesh", "HM", "", "Demo", "0_Params", GH_ParamAccess.tree)
        {
        }
        public Param_HexMesh(IGH_InstanceDescription tag) : base(tag)
        {
        }

        public Param_HexMesh(IGH_InstanceDescription tag, GH_ParamAccess access) : base(tag, access)
        {
        }

        public Param_HexMesh(string name, string nickname, string description, GH_ParamAccess access) : base(name, nickname, description, "Demo", "Mesh", access)
        {
        }

        public override Guid ComponentGuid => new Guid("10f558b7-e786-482b-b0c2-de589b284bfd");
        public bool Hidden { get; set; }

        public bool IsPreviewCapable => true;

        public BoundingBox ClippingBox => Preview_ComputeClippingBox();

        public void DrawViewportMeshes(IGH_PreviewArgs args)
        {
            Preview_DrawMeshes(args);
        }

        public void DrawViewportWires(IGH_PreviewArgs args)
        {
            Preview_DrawWires(args);
        }
    }
}
