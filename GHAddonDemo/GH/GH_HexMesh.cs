﻿using System.Linq;
using GHAddonDemo.Core;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Rhino.Geometry;

namespace GHAddonDemo.GH
{
    public class GH_HexMesh : GH_GeometricGoo<HexMesh>, IGH_PreviewData
    {
        private Line[] _previewLines;

        public GH_HexMesh()
        {
        }

        public GH_HexMesh(HexMesh internal_data) : base(internal_data)
        {
            UpdatePreviewGeometries();
        }

        public override string TypeName => "GH_HexMesh";

        public override string TypeDescription => "Goo for HexMesh class.";

        public override bool IsValid => Value != null && Value.IsValid;
        public override BoundingBox Boundingbox => new BoundingBox(Value.Nodes);
        public BoundingBox ClippingBox => Boundingbox;

        public override IGH_Goo Duplicate()
        {
            return new GH_HexMesh(new HexMesh(Value));
        }

        public override IGH_GeometricGoo DuplicateGeometry()
        {
            return new GH_HexMesh(new HexMesh(Value));
        }

        public override BoundingBox GetBoundingBox(Transform xform)
        {
            var bbox = Boundingbox;
            bbox.Transform(xform);
            return bbox;
        }

        public override IGH_GeometricGoo Transform(Transform xform)
        {
            var mesh = Value.DeepCopy();

            for (int i = 0; i < mesh.NodeCount; i++)
            {
                var node = mesh.Nodes[i];
                node.Transform(xform);
                mesh.Nodes[i] = node;
            }
            return new GH_HexMesh(mesh);
        }
        public override IGH_GeometricGoo Morph(SpaceMorph xmorph)
        {
            var mesh = Value.DeepCopy();

            var morphedPts = mesh.Nodes.Select(node => xmorph.MorphPoint(node)).ToArray();
            mesh.Nodes.Clear();
            mesh.Nodes.AddRange(morphedPts);

            return new GH_HexMesh(mesh);
        }

        public override string ToString()
        {
            return Value.ToString();
        }
        public override bool CastFrom(object source)
        {
            if (source is HexMesh hexMesh)
            {
                Value = hexMesh.DeepCopy();
                UpdatePreviewGeometries();
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool CastTo<Q>(ref Q target)
        {
            target = default;
            if (typeof(Q).IsAssignableFrom(typeof(HexMesh)))
            {
                target = (Q)(object)Value;
                return true;
            }
            else if (typeof(Q).IsAssignableFrom(typeof(GH_Mesh)))
            {
                target = (Q)(object)new GH_Mesh(Value.ToSurfaceMesh());
                return true;
            }
            else
            {
                return false;
            }
        }

        private void UpdatePreviewGeometries()
        {
            if (Value == null) _previewLines = new Line[] { };
            _previewLines = Value.ToWireLines();
        }
        public void DrawViewportWires(GH_PreviewWireArgs args)
        {
            args.Pipeline.DrawLines(_previewLines, args.Color);
        }

        public void DrawViewportMeshes(GH_PreviewMeshArgs args)
        {
        }
    }
}
