﻿using System;
using System.Collections.Generic;
using GHAddonDemo.Core;
using Grasshopper.Kernel;
using Rhino.Geometry;

namespace GHAddonDemo.GH
{
    public class HexMeshFromSurfaceOffset : GH_Component
    {
        public HexMeshFromSurfaceOffset() : base("HexMeshFromSurfaceOffset", "HexMeshFromSrfOffset", "Define HexMesh from offset of Surface.", "Demo", "1_Mesh")
        {
        }

        public override Guid ComponentGuid => new Guid("14b945cc-c4bd-4c88-854b-d5b4308616a6");

        protected override void RegisterInputParams(GH_InputParamManager pManager)
        {
            pManager.AddSurfaceParameter("Surface", "S", "Untrimmed Surface to generate HexMesh.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Offset", "O", "Offset distance", GH_ParamAccess.item, 1);
            pManager.AddIntegerParameter("UDivision", "U", "Division along U-direction", GH_ParamAccess.item, 10);
            pManager.AddIntegerParameter("VDivision", "V", "Division along V-direction", GH_ParamAccess.item, 10);
            pManager.AddIntegerParameter("WDivision", "W", "Division along W-direction", GH_ParamAccess.item, 1);
            pManager.AddBooleanParameter("Flip", "F", "Flip offset direction", GH_ParamAccess.item, false);
        }

        protected override void RegisterOutputParams(GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Param_HexMesh("HesMesh", "HM", "Generated HexMesh", GH_ParamAccess.item));
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Surface srf = default;
            double offset = default;
            int uDiv = default;
            int vDiv = default;
            int wDiv = default;
            bool flip = default;

            if (!DA.GetData(0, ref srf)) return;
            if (!DA.GetData(1, ref offset)) return;
            if (!DA.GetData(2, ref uDiv)) return;
            if (!DA.GetData(3, ref vDiv)) return;
            if (!DA.GetData(4, ref wDiv)) return;
            if (!DA.GetData(5, ref flip)) return;

            var uDomain = srf.Domain(0);
            var vDomain = srf.Domain(1);
            var ptCount = (uDiv + 1) * (vDiv + 1) * (wDiv + 1);
            Point3d[] pts = new Point3d[ptCount];

            for (int iu = 0; iu < uDiv + 1; iu++)
                for (int iv = 0; iv < vDiv + 1; iv++)
                {
                    var u = uDomain.Min + uDomain.Length * iu / uDiv;
                    var v = vDomain.Min + vDomain.Length * iv / vDiv;
                    var pt = srf.PointAt(u, v);
                    var normal = srf.NormalAt(u, v);
                    normal.Unitize();
                    if (flip)
                        normal *= -1;
                    for (int iw = 0; iw < wDiv + 1; iw++)
                    {
                        pts[ToSerialIdx(iu, iv, iw, uDiv + 1, vDiv + 1)] = pt + normal * offset * ((double)iw / wDiv);
                    }
                }

            List<HexElement> elements = new List<HexElement>();

            for (int iu = 0; iu < uDiv; iu++)
                for (int iv = 0; iv < vDiv; iv++)
                    for (int iw = 0; iw < wDiv; iw++)
                    {
                        elements.Add(new HexElement(new int[]
                        {
                            ToSerialIdx(iu, iv, iw, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu+1, iv, iw, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu+1, iv+1, iw, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu, iv+1, iw, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu, iv, iw+1, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu+1, iv, iw+1, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu+1, iv+1, iw+1, uDiv + 1, vDiv + 1),
                            ToSerialIdx(iu, iv+1, iw+1, uDiv + 1, vDiv + 1),
                        }));
                    }

            var hexmesh = new HexMesh(pts, elements);

            DA.SetData(0, hexmesh);
        }

        private int ToSerialIdx(int uIdx, int vIdx, int wIdx, int uCount, int vCount)
        {
            return (uCount * vCount) * wIdx + uCount * vIdx + uIdx;
        }
    }
}
