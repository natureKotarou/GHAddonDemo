﻿using Rhino.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHAddonDemo.Core
{

    public class HexMesh
    {
        public HexMesh(IEnumerable<Point3d> nodes = null, IEnumerable<HexElement> elements = null)
        {
            Nodes = nodes is null ? new List<Point3d>() : nodes.ToList();
            Elements = elements is null ? new List<HexElement>() : elements.ToList();
        }
        public HexMesh(HexMesh other)
        {
            if (other is null) throw new ArgumentNullException(nameof(other));
            if (!other.IsValid) throw new ArgumentException(nameof(other));
            Nodes = other.Nodes.Select(node => new Point3d(node)).ToList();
            Elements = other.Elements.Select(elem => new HexElement(elem)).ToList();
        }

        public List<Point3d> Nodes { get; private set; }
        public List<HexElement> Elements { get; private set; }
        public int NodeCount => Nodes.Count;
        public int ElementCount => Elements.Count;
        public bool IsValid => Nodes != null && Elements != null && Elements.All(elem => elem.Validate(this));
        public IndexSetWithOrderIndependentHash[] GetIndexSetOfEdges()
        {
            HashSet<IndexSetWithOrderIndependentHash> hashset = new HashSet<IndexSetWithOrderIndependentHash>();
            foreach (var set in Elements.SelectMany(element => element.GetIndexSetOfEdges()))
            {
                if (!hashset.Contains(set))
                    hashset.Add(set);
            }
            return hashset.ToArray();
        }
        public IndexSetWithOrderIndependentHash[] GetIndexSetOfFaces(bool getOnlyBoundary = true)
        {
            var faces = Elements.SelectMany(element => element.GetIndexSetOfFaces());
            if (getOnlyBoundary)
                return faces.GroupBy(id => id.GetHashCode()).Where(x => x.Count() == 1).Select(x => x.Single()).ToArray();
            else
                return faces.ToArray();
        }

        public Line[] ToWireLines()
        {
            var edges = GetIndexSetOfEdges();
            return edges.Select(edge => new Line(Nodes[edge.Indices[0]], Nodes[edge.Indices[1]])).ToArray();
        }
        public Mesh ToSurfaceMesh()
        {
            var faces = GetIndexSetOfFaces(true);
            var mesh = new Mesh();
            mesh.Vertices.AddVertices(Nodes);
            mesh.Faces.AddFaces(faces.Select(face => new MeshFace(face[0], face[1], face[2], face[3])));
            mesh.Vertices.CullUnused();

            mesh.RebuildNormals();
            if (mesh.Volume() < 0)
            {
                mesh.Flip(true, true, true);
            }
            return mesh;
        }
        public HexMesh DeepCopy()
        {
            return new HexMesh(this);
        }

        public override string ToString()
        {
            return $"HexMesh Nodes:{NodeCount} Elements:{ElementCount}";
        }
    }
}
