﻿using System;
using System.Linq;

namespace GHAddonDemo.Core
{
    /// <summary>
    /// Element class to represent 8 node hexahedron
    /// 3----------2     
    /// |\     ^   |\    
    /// | \    |   | \   
    /// |  \   |   |  \  
    /// |   7------+---6 
    /// |   |  +-- |-- | 
    /// 0---+---\--1   | 
    ///  \  |    \  \  | 
    ///   \ |     \  \ | 
    ///    \|      w  \| 
    ///     4----------5 
    /// </summary>
    public class HexElement
    {
        public HexElement(int[] nodeIndices)
        {
            if (nodeIndices is null) throw new ArgumentNullException(nameof(nodeIndices));
            if (nodeIndices.Length != 8) throw new ArgumentException(nameof(nodeIndices));
            if (nodeIndices.Any(index => index < 0)) throw new ArgumentException(nameof(nodeIndices));
            NodeIndices = nodeIndices;
        }
        public HexElement(HexElement other)
        {
            if (other.NodeIndices is null) throw new ArgumentNullException(nameof(other.NodeIndices));
            if (other.NodeIndices.Length != 8) throw new ArgumentException(nameof(other.NodeIndices));
            if (other.NodeIndices.Any(index => index < 0)) throw new ArgumentException(nameof(other.NodeIndices));
            NodeIndices = new int[8];
            Array.Copy(other.NodeIndices, NodeIndices, 8);
        }

        public int[] NodeIndices { get; }
        public int this[int index] => NodeIndices[index];
        public IndexSetWithOrderIndependentHash[] GetIndexSetOfEdges()
        {
            return new IndexSetWithOrderIndependentHash[]
            {
                new IndexSetWithOrderIndependentHash(NodeIndices[0], NodeIndices[1]),
                new IndexSetWithOrderIndependentHash(NodeIndices[1], NodeIndices[2]),
                new IndexSetWithOrderIndependentHash(NodeIndices[2], NodeIndices[3]),
                new IndexSetWithOrderIndependentHash(NodeIndices[3], NodeIndices[0]),
                new IndexSetWithOrderIndependentHash(NodeIndices[4], NodeIndices[5]),
                new IndexSetWithOrderIndependentHash(NodeIndices[5], NodeIndices[6]),
                new IndexSetWithOrderIndependentHash(NodeIndices[6], NodeIndices[7]),
                new IndexSetWithOrderIndependentHash(NodeIndices[7], NodeIndices[4]),
                new IndexSetWithOrderIndependentHash(NodeIndices[0], NodeIndices[4]),
                new IndexSetWithOrderIndependentHash(NodeIndices[1], NodeIndices[5]),
                new IndexSetWithOrderIndependentHash(NodeIndices[2], NodeIndices[6]),
                new IndexSetWithOrderIndependentHash(NodeIndices[3], NodeIndices[7]),
            };
        }
        public IndexSetWithOrderIndependentHash[] GetIndexSetOfFaces()
        {
            return new IndexSetWithOrderIndependentHash[]
            {
                new IndexSetWithOrderIndependentHash(NodeIndices[0], NodeIndices[3], NodeIndices[2], NodeIndices[1]),
                new IndexSetWithOrderIndependentHash(NodeIndices[4], NodeIndices[5], NodeIndices[6], NodeIndices[7]),
                new IndexSetWithOrderIndependentHash(NodeIndices[0], NodeIndices[4], NodeIndices[7], NodeIndices[3]),
                new IndexSetWithOrderIndependentHash(NodeIndices[0], NodeIndices[1], NodeIndices[5], NodeIndices[4]),
                new IndexSetWithOrderIndependentHash(NodeIndices[1], NodeIndices[2], NodeIndices[6], NodeIndices[5]),
                new IndexSetWithOrderIndependentHash(NodeIndices[2], NodeIndices[3], NodeIndices[7], NodeIndices[6]),
            };
        }
        public HexElement DeepCopy()
        {
            return new HexElement(this);
        }

        public bool Validate(in HexMesh mesh)
        {
            var nodeCount = mesh.NodeCount;
            return NodeIndices != null && NodeIndices.Length == 8 && NodeIndices.All(idx => idx >= 0 && idx < nodeCount);
        }
    }
}
