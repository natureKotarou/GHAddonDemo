﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GHAddonDemo.Core
{
    public class IndexSetWithOrderIndependentHash : IEquatable<IndexSetWithOrderIndependentHash>
    {
        public IndexSetWithOrderIndependentHash(params int[] indices)
        {
            Indices = indices;
        }

        public int[] Indices { get; }

        public int this[int index] => Indices[index];

        public override bool Equals(object obj)
        {
            return obj is IndexSetWithOrderIndependentHash set &&
                set.Indices != null &&
                this.Indices != null &&
                GetHashCode() == set.GetHashCode();
        }

        public bool Equals(IndexSetWithOrderIndependentHash other)
        {
            return other.Indices != null &&
                this.Indices != null &&
                GetHashCode() == other.GetHashCode();
        }

        public override int GetHashCode()
        {
            int hashCode = -1817952719;
            foreach (int idx in Indices.OrderBy(x => x))
                hashCode = hashCode * -1521134295 + idx.GetHashCode();
            return hashCode;
        }
    }
}
